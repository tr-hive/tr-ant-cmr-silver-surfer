/// <reference path="../typings/tsd.d.ts"/>
/// <reference path="../src/buy-handler.ts"/>
var chai = require('chai');
var handler = require('../src/buy-handler');
var Rx = require('rx/index');
var expect = chai.expect;
var onNext = Rx.ReactiveTest.onNext, onCompleted = Rx.ReactiveTest.onCompleted, subscribe = Rx.ReactiveTest.subscribe;
describe("simple buy", function () {
    it("q1 - n1 - q2 - buy", function () {
        var notif = {
            key: "0",
            issuer: "test",
            date: (new Date()).toISOString(),
            type: "INotifSilverSurferData",
            data: {
                ticket: "SBER",
                oper: "buy",
                stop: null,
                force: null
            }
        };
        var quote1 = {
            ticket: "SBER",
            latestPrice: 100,
            bid: 100,
            ask: 100
        };
        var quote2 = {
            ticket: "SBER",
            latestPrice: 90,
            bid: 90,
            ask: 90
        };
        var scheduler = new Rx.TestScheduler();
        var sss = scheduler.createHotObservable(onNext(200, notif), onCompleted(700));
        var qs = scheduler.createHotObservable(onNext(100, [quote1]), onNext(300, [quote2]), onCompleted(700));
        var opts = {
            silverSurferStream: sss,
            quotesStream: qs,
            endOfDay: 16,
            buyPercent: 1,
            cancelBuyPercent: 1
        };
        var res = scheduler.startWithTiming(function () {
            return handler.handleBuy(opts, scheduler);
        }, 0, 0, 1000);
        var expected = {
            type: handler.ResultType.buy,
            notif: notif,
            startQuote: quote1,
            quote: quote2,
            reason: "buy when original price 100 drops by 1 percent; current price is 90",
            expire: null,
            quantity: 10
        };
        expect(res.messages).eqls([
            onNext(300, expected),
            onCompleted(700)
        ]);
    });
    it("q1 - n1 - q2 - cancel buy", function () {
        var notif = {
            key: "0",
            issuer: "test",
            date: (new Date()).toISOString(),
            type: "INotifSilverSurferData",
            data: {
                ticket: "SBER",
                oper: "buy",
                stop: null,
                force: null
            }
        };
        var quote1 = {
            ticket: "SBER",
            latestPrice: 100,
            bid: 100,
            ask: 100
        };
        var quote2 = {
            ticket: "SBER",
            latestPrice: 110,
            bid: 110,
            ask: 110
        };
        var scheduler = new Rx.TestScheduler();
        var sss = scheduler.createHotObservable(onNext(200, notif), onCompleted(700));
        var qs = scheduler.createHotObservable(onNext(100, [quote1]), onNext(300, [quote2]), onCompleted(700));
        var opts = {
            silverSurferStream: sss,
            quotesStream: qs,
            endOfDay: 16,
            buyPercent: 1,
            cancelBuyPercent: 1
        };
        var res = scheduler.startWithTiming(function () {
            return handler.handleBuy(opts, scheduler);
        }, 0, 0, 1000);
        var expected = {
            type: handler.ResultType.cancel,
            notif: notif,
            startQuote: quote1,
            quote: quote2,
            reason: "cancel buy when current price 110 become bigger than original price 100 by 1 percent",
            expire: null,
            quantity: 0
        };
        expect(res.messages).eqls([
            onNext(300, expected),
            onCompleted(700)
        ]);
    });
});
