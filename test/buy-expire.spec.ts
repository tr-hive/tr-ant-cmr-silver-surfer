/// <reference path="../typings/tsd.d.ts"/>
/// <reference path="../src/buy-handler.ts"/>

import * as chai from 'chai'
import * as handler from '../src/buy-handler'
import * as Rx from 'rx/index'
import * as moment from 'moment'
import * as utils from '../node_modules/tr-ant-utils'

var expect = chai.expect;

var onNext = Rx.ReactiveTest.onNext,
    onCompleted = Rx.ReactiveTest.onCompleted,
    subscribe = Rx.ReactiveTest.subscribe;

describe("buy expire",  () => {

	it("n1 - eod - expired",  () => {

		var notif: utils.INotif<utils.INotifSilverSurferData> = {
			key: "0",
			issuer: "test",
			date: (new Date()).toISOString(),
			type : "INotifSilverSurferData",
			data : {
				ticket: "SBER",
				oper: "buy",
				stop: null,
				force: null					
			}			
		}
		
		var scheduler = new Rx.TestScheduler();
		
		var completedTime = 24 * 60 * 60 * 1000;
		var expireTime = 16 * 60 * 60 * 1000;

		var sss = scheduler.createHotObservable<utils.INotif<utils.INotifSilverSurferData>>(
			onNext(200, notif),
			onCompleted(completedTime)
			);


		var qs = scheduler.createHotObservable<utils.IQuote[]>(			
			onCompleted(completedTime)
		);
			
		var opts : handler.IBuyOpts = {
			silverSurferStream: sss,
			quotesStream: qs,
			endOfDay: 16,
			buyPercent: 1,
			cancelBuyPercent: 1
		};		

		var res = scheduler.startWithTiming(() =>
			handler.handleBuy(opts, scheduler)
		, 0, 0, completedTime);
		
		var expected : handler.IResult = {
			type: handler.ResultType.expired,
			notif : notif,
			startQuote: null,
			quote: null,
			expire: expireTime - 200, 
			reason: "Canceled by end of the day",
			quantity: 0		
		}
		
		expect(res.messages).eqls(
			[
				onNext(expireTime + 1, expected),
				onCompleted(completedTime)
			]
			);
	});
		
	it.only("q1 - n1 - eod - expired",  () => {
		
		var completedTime = 24 * 60 * 60 * 1000;
		var expireTime = 16 * 60 * 60 * 1000;
		
		var notif: utils.INotif<utils.INotifSilverSurferData> = {
			key: "0",
			issuer: "test",
			date: (new Date()).toISOString(),
			type : "INotifSilverSurferData",
			data : {
				ticket: "SBER",
				oper: "buy",
				stop: null,
				force: null					
			}			
		}
		
		var quote1 = {
				ticket: "SBER",
				latestPrice: 100,
				bid: 100,
				ask: 100				
			};

		var scheduler = new Rx.TestScheduler();

		var sss = scheduler.createHotObservable<utils.INotif<utils.INotifSilverSurferData>>(
			onNext(200, notif),
			onCompleted(completedTime)
			);

		var qs = scheduler.createHotObservable<utils.IQuote[]>(
			onNext(100, [quote1]),			
			onCompleted(completedTime)
		);
			
		var opts : handler.IBuyOpts = {
			silverSurferStream: sss,
			quotesStream: qs,
			endOfDay: 16,
			buyPercent: 1,
			cancelBuyPercent: 1
		};		

		var res = scheduler.startWithTiming(() =>
			handler.handleBuy(opts, scheduler)
		, 0, 0, completedTime);
		
		var expected : handler.IResult = {
			type: handler.ResultType.expired,
			notif : notif,
			startQuote: quote1,
			quote: null,
			expire: expireTime - 200, 
			reason: "Canceled by end of the day",
			quantity: 0		
		}
		
				
		expect(res.messages).eqls(
			[
				onNext(expireTime, expected),
				onCompleted(completedTime)
			]
			);
	});
	
	
	it("n1 - n2 - q1 - eod - expired",  () => {
		
		var completedTime = 24 * 60 * 60 * 1000;
		var expireTime = 16 * 60 * 60 * 1000;
		
		var notif1: utils.INotif<utils.INotifSilverSurferData> = {
			key: "0",
			issuer: "test",
			date: (new Date()).toISOString(),
			type : "INotifSilverSurferData",
			data : {
				ticket: "SBER",
				oper: "buy",
				stop: null,
				force: null					
			}			
		}
		
		var notif2: utils.INotif<utils.INotifSilverSurferData> = {
			key: "0",
			issuer: "test",
			date: (new Date()).toISOString(),
			type : "INotifSilverSurferData",
			data : {
				ticket: "HYDR",
				oper: "buy",
				stop: null,
				force: null					
			}			
		}
				
		var quote1 = {
				ticket: "SBER",
				latestPrice: 100,
				bid: 100,
				ask: 100				
			};

		var scheduler = new Rx.TestScheduler();

		var sss = scheduler.createHotObservable<utils.INotif<utils.INotifSilverSurferData>>(
			onNext(200, notif1),
			onNext(300, notif2),
			onCompleted(completedTime)
			);

		var qs = scheduler.createHotObservable<utils.IQuote[]>(
			onNext(500, [quote1]),			
			onCompleted(completedTime)
		);
			
		var opts : handler.IBuyOpts = {
			silverSurferStream: sss,
			quotesStream: qs,
			endOfDay: 16,
			buyPercent: 1,
			cancelBuyPercent: 1
		};		

		var res = scheduler.startWithTiming(() =>
			handler.handleBuy(opts, scheduler)
		, 0, 0, completedTime);
		
		var expected : handler.IResult[] = [
			{
				type: handler.ResultType.expired,
				notif : notif1,
				startQuote: quote1,
				quote: null,
				expire: expireTime - 200, 
				reason: "Canceled by end of the day",
				quantity: 0
			},
			{
				type: handler.ResultType.expired,
				notif : notif2,
				startQuote: null,
				quote: null,
				expire: expireTime - 200 - 100, 
				reason: "Canceled by end of the day",
				quantity: 0
			}			
		]
		
				
		expect(res.messages).eqls(
			[
				onNext(expireTime, expected[0]),
				onNext(expireTime + 1, expected[1]),
				onCompleted(completedTime)
			]
			);
	});
		
	it("q1 - n1 - q2 - eod - expired",  () => {
		
		var completedTime = 24 * 60 * 60 * 1000;
		var expireTime = 16 * 60 * 60 * 1000;
		
		var notif: utils.INotif<utils.INotifSilverSurferData> = {
			key: "0",
			issuer: "test",
			date: (new Date()).toISOString(),
			type : "INotifSilverSurferData",
			data : {
				ticket: "SBER",
				oper: "buy",
				stop: null,
				force: null					
			}			
		}
		
		var quote1 = {
				ticket: "SBER",
				latestPrice: 100,
				bid: 100,
				ask: 100				
			};
			
		var quote2 = {
				ticket: "SBER",
				latestPrice: 100.1,
				bid: 100.1,
				ask: 100.1				
			};


		var scheduler = new Rx.TestScheduler();

		var sss = scheduler.createHotObservable<utils.INotif<utils.INotifSilverSurferData>>(
			onNext(200, notif),
			onCompleted(completedTime)
			);

		var qs = scheduler.createHotObservable<utils.IQuote[]>(
			onNext(100, [quote1]),			
			onNext(300, [quote2]),			
			onCompleted(completedTime)
		);
			
		var opts : handler.IBuyOpts = {
			silverSurferStream: sss,
			quotesStream: qs,
			endOfDay: 16,
			buyPercent: 1,
			cancelBuyPercent: 1
		};		

		var res = scheduler.startWithTiming(() =>
			handler.handleBuy(opts, scheduler)
		, 0, 0, completedTime);
		
		var expected : handler.IResult = {
			type: handler.ResultType.expired,
			notif : notif,
			startQuote: quote1,
			quote: null,
			expire: expireTime - 200, 
			reason: "Canceled by end of the day",
			quantity: 0		
		}
		
		expect(res.messages).eqls(
			[
				onNext(expireTime, expected),
				onCompleted(completedTime)
			]
			);
	});
	
	it("q1 - n1 - q2 - n2 - eod - expired",  () => {
		
		var completedTime = 24 * 60 * 60 * 1000;
		var expireTime = 16 * 60 * 60 * 1000;
		
		var notif1: utils.INotif<utils.INotifSilverSurferData> = {
			key: "0",
			issuer: "test",
			date: (new Date()).toISOString(),
			type : "INotifSilverSurferData",
			data : {
				ticket: "SBER",
				oper: "buy",
				stop: null,
				force: null					
			}			
		}
		
		var notif2: utils.INotif<utils.INotifSilverSurferData> = {
			key: "0",
			issuer: "test",
			date: (new Date()).toISOString(),
			type : "INotifSilverSurferData",
			data : {
				ticket: "SBER",
				oper: "buy",
				stop: null,
				force: null					
			}			
		}		
		
		var quote1 = {
				ticket: "SBER",
				latestPrice: 100,
				bid: 100,
				ask: 100				
			};
			
		var quote2 = {
				ticket: "SBER",
				latestPrice: 100.1,
				bid: 100.1,
				ask: 100.1				
			};


		var scheduler = new Rx.TestScheduler();

		var sss = scheduler.createHotObservable<utils.INotif<utils.INotifSilverSurferData>>(
			onNext(200, notif1),
			onNext(400, notif2),
			onCompleted(completedTime)
			);

		var qs = scheduler.createHotObservable<utils.IQuote[]>(
			onNext(100, [quote1]),			
			onNext(300, [quote2]),			
			onCompleted(completedTime)
		);
			
		var opts : handler.IBuyOpts = {
			silverSurferStream: sss,
			quotesStream: qs,
			endOfDay: 16,
			buyPercent: 1,
			cancelBuyPercent: 1
		};		

		var res = scheduler.startWithTiming(() =>
			handler.handleBuy(opts, scheduler)
		, 0, 0, completedTime);
		
		var expected : handler.IResult[] = [
		{
			type: handler.ResultType.expired,
			notif : notif1,
			startQuote: quote1,
			quote: null,
			expire: expireTime - 200, 
			reason: "Canceled by end of the day",
			quantity: 0		
		}, 			 
		{
			type: handler.ResultType.expired,
			notif : notif2,
			startQuote: quote2,
			quote: null,
			expire: expireTime - 200, 
			reason: "Canceled by end of the day",
			quantity: 0		
		} ]
				
		expect(res.messages).eqls(
			[
				onNext(expireTime, expected[0]),
				onNext(expireTime, expected[1]),
				onCompleted(completedTime)
			]
			);
	});
	

	it("q1 - n1 - eod - q2 - expired",  () => {
		
		var completedTime = 24 * 60 * 60 * 1000;
		var expireTime = 16 * 60 * 60 * 1000;
		
		var notif: utils.INotif<utils.INotifSilverSurferData> = {
			key: "0",
			issuer: "test",
			date: (new Date()).toISOString(),
			type : "INotifSilverSurferData",
			data : {
				ticket: "SBER",
				oper: "buy",
				stop: null,
				force: null					
			}			
		}
		
		var quote1 = {
				ticket: "SBER",
				latestPrice: 100,
				bid: 100,
				ask: 100				
			};
			
		var quote2 = {
				ticket: "SBER",
				latestPrice: 120,
				bid: 120,
				ask: 120				
			};


		var scheduler = new Rx.TestScheduler();

		var sss = scheduler.createHotObservable<utils.INotif<utils.INotifSilverSurferData>>(
			onNext(200, notif),
			onCompleted(completedTime)
			);

		var qs = scheduler.createHotObservable<utils.IQuote[]>(
			onNext(100, [quote1]),			
			onNext(expireTime + 300, [quote2]),			
			onCompleted(completedTime)
		);
			
		var opts : handler.IBuyOpts = {
			silverSurferStream: sss,
			quotesStream: qs,
			endOfDay: 16,
			buyPercent: 1,
			cancelBuyPercent: 1
		};		

		var res = scheduler.startWithTiming(() =>
			handler.handleBuy(opts, scheduler)
		, 0, 0, completedTime);
		
		var expected : handler.IResult = {
			type: handler.ResultType.expired,
			notif : notif,
			startQuote: quote1,
			quote: null,
			expire: expireTime - 200, 
			reason: "Canceled by end of the day",
			quantity: 0		
		}
		
		expect(res.messages).eqls(
			[
				onNext(expireTime, expected),
				onCompleted(completedTime)
			]
			);
	});
	
});		