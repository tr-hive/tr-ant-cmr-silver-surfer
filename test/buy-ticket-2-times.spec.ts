/// <reference path="../typings/tsd.d.ts"/>
/// <reference path="../src/buy-handler.ts"/>

import * as chai from 'chai'
import * as handler from '../src/buy-handler'
import * as Rx from 'rx/index'
import * as utils from '../node_modules/tr-ant-utils'

var expect = chai.expect;

var onNext = Rx.ReactiveTest.onNext,
    onCompleted = Rx.ReactiveTest.onCompleted,
    subscribe = Rx.ReactiveTest.subscribe;

describe("buy ticket 2 times",  () => {

	it("q1 - n1 - q2 - n2 - q3 - buy first cancel second",  () => {

		var notif: utils.INotif<utils.INotifSilverSurferData> = {
			key: "0",
			issuer: "test",
			date: (new Date()).toISOString(),
			type : "INotifSilverSurferData",
			data : {
				ticket: "SBER",
				oper: "buy",
				stop: null,
				force: null					
			}			
		}
		
		var quote1 = {
				ticket: "SBER",
				latestPrice: 100,
				bid: 100,
				ask: 100				
			};

		var quote2 = {
				ticket: "SBER",
				latestPrice: 90,
				bid: 90,
				ask: 90				
			};

		var quote3 = {
				ticket: "SBER",
				latestPrice: 110,
				bid: 110,
				ask: 110				
			};
			
		var scheduler = new Rx.TestScheduler();

		var sss = scheduler.createHotObservable<utils.INotif<utils.INotifSilverSurferData>>(
			onNext(200, notif),
			onNext(400, notif), 
			onCompleted(1000)
			);

		var qs = scheduler.createHotObservable<utils.IQuote[]>(
			onNext(100, [quote1]),
			onNext(300, [quote2]),			
			onNext(500, [quote3]),
			onCompleted(1000)
		);
			
		var opts : handler.IBuyOpts = {
			silverSurferStream: sss,
			quotesStream: qs,
			endOfDay: 16,
			buyPercent: 1,
			cancelBuyPercent: 1
		};		

		var res = scheduler.startWithTiming(() =>
			handler.handleBuy(opts, scheduler)
		, 0, 0, 1200);
		
		var expected_a : handler.IResult = {
			type: handler.ResultType.buy,
			notif : notif,
			startQuote: quote1,
			quote: quote2,
			reason: "buy when original price 100 drops by 1 percent; current price is 90",
			expire: null,
			quantity: 10		
		}
		var expected_b : handler.IResult = {
			type: handler.ResultType.cancel,
			notif : notif,
			startQuote: quote2,
			quote: quote3,
			reason: "cancel buy when current price 110 become bigger than original price 90 by 1 percent",
			expire: null,
			quantity: 0		
		}		
		
		//console.log(res.messages);
				
		expect(res.messages).eqls(
			[
				onNext(300, expected_a),
				onNext(500, expected_b),
				onCompleted(1000)
			]
			);
	});
			
});		