/// <reference path="../typings/tsd.d.ts"/>
/// <reference path="../src/buy-handler.ts"/>

import * as chai from 'chai'
import * as handler from '../src/buy-handler'
import * as utils from '../node_modules/tr-ant-utils'
import * as Rx from 'rx/index'

var expect = chai.expect;

var onNext = Rx.ReactiveTest.onNext,
    onCompleted = Rx.ReactiveTest.onCompleted,
    subscribe = Rx.ReactiveTest.subscribe;

describe("buy 2 tickets",  () => {

	it("qa1 - na1 - qa2 - qb1 - nb2 - qb2 - buy both",  () => {

		var notif_a: utils.INotif<utils.INotifSilverSurferData> = {
			key: "0",
			issuer: "test",
			date: (new Date()).toISOString(),
			type : "INotifSilverSurferData",
			data : {
				ticket: "SBER",
				oper: "buy",
				stop: null,
				force: null					
			}			
		}
		
		var notif_b: utils.INotif<utils.INotifSilverSurferData> = {
			key: "1",
			issuer: "test",
			date: (new Date()).toISOString(),
			type : "INotifSilverSurferData",
			data : {
				ticket: "MTSS",
				oper: "buy",
				stop: null,
				force: null					
			}			
		}
				
		var quote_a1 = {
				ticket: "SBER",
				latestPrice: 100,
				bid: 100,
				ask: 100				
			};

		var quote_a2 = {
				ticket: "SBER",
				latestPrice: 90,
				bid: 90,
				ask: 90				
			};

		var quote_b1 = {
				ticket: "MTSS",
				latestPrice: 100,
				bid: 100,
				ask: 100				
			};

		var quote_b2 = {
				ticket: "MTSS",
				latestPrice: 90,
				bid: 90,
				ask: 90				
			};
		
		var scheduler = new Rx.TestScheduler();

		var sss = scheduler.createHotObservable<utils.INotif<utils.INotifSilverSurferData>>(
			onNext(200, notif_a),
			onNext(600, notif_b), //??? separate test
			onCompleted(1000)
			);


		var qs = scheduler.createHotObservable<utils.IQuote[]>(
			onNext(100, [quote_a1]),
			onNext(300, [quote_a2]),			
			onNext(500, [quote_b1]),
			onNext(700, [quote_b2]),
			onCompleted(1000)
		);
			
		var opts : handler.IBuyOpts = {
			silverSurferStream: sss,
			quotesStream: qs,
			endOfDay: 16,
			buyPercent: 1,
			cancelBuyPercent: 1
		};		

		var res = scheduler.startWithTiming(() =>
			handler.handleBuy(opts, scheduler)
		, 0, 0, 1200);
		
		var expected_a : handler.IResult = {
			type: handler.ResultType.buy,
			notif : notif_a,
			startQuote: quote_a1,
			quote: quote_a2,
			reason: "buy when original price 100 drops by 1 percent; current price is 90",
			expire: null,
			quantity: 10		
		}
		var expected_b : handler.IResult = {
			type: handler.ResultType.buy,
			notif : notif_b,
			startQuote: quote_b1,
			quote: quote_b2,
			reason: "buy when original price 100 drops by 1 percent; current price is 90",
			expire: null,
			quantity: 10		
		}		
		
		//console.log(res.messages);
				
		expect(res.messages).eqls(
			[
				onNext(300, expected_a),
				onNext(700, expected_b),
				onCompleted(1000)
			]
			);
	});
		
	it("qa1 - na1 - qa2 - qb1 - nb2 - qb2 - cancel both",  () => {

		var notif_a: utils.INotif<utils.INotifSilverSurferData> = {
			key: "0",
			issuer: "test",
			date: (new Date()).toISOString(),
			type : "INotifSilverSurferData",
			data : {
				ticket: "SBER",
				oper: "buy",
				stop: null,
				force: null					
			}			
		}
		
		var notif_b: utils.INotif<utils.INotifSilverSurferData> = {
			key: "1",
			issuer: "test",
			date: (new Date()).toISOString(),
			type : "INotifSilverSurferData",
			data : {
				ticket: "MTSS",
				oper: "buy",
				stop: null,
				force: null					
			}			
		}
				
		var quote_a1 = {
				ticket: "SBER",
				latestPrice: 100,
				bid: 100,
				ask: 100				
			};

		var quote_a2 = {
				ticket: "SBER",
				latestPrice: 110,
				bid: 110,
				ask: 110				
			};

		var quote_b1 = {
				ticket: "MTSS",
				latestPrice: 100,
				bid: 100,
				ask: 100				
			};

		var quote_b2 = {
				ticket: "MTSS",
				latestPrice: 110,
				bid: 110,
				ask: 110				
			};
		
		var scheduler = new Rx.TestScheduler();

		var sss = scheduler.createHotObservable<utils.INotif<utils.INotifSilverSurferData>>(
			onNext(200, notif_a),
			onNext(600, notif_b), 
			onCompleted(1000)
			);


		var qs = scheduler.createHotObservable<utils.IQuote[]>(
			onNext(100, [quote_a1]),
			onNext(300, [quote_a2]),			
			onNext(500, [quote_b1]),
			onNext(700, [quote_b2]),
			onCompleted(1000)
		);
			
		var opts : handler.IBuyOpts = {
			silverSurferStream: sss,
			quotesStream: qs,
			endOfDay: 16,
			buyPercent: 1,
			cancelBuyPercent: 1
		};		

		var res = scheduler.startWithTiming(() =>
			handler.handleBuy(opts, scheduler)
		, 0, 0, 1200);
		
		var expected_a : handler.IResult = {
			type: handler.ResultType.cancel,
			notif : notif_a,
			startQuote: quote_a1,
			quote: quote_a2,
			reason: "cancel buy when current price 110 become bigger than original price 100 by 1 percent",
			expire: null,
			quantity: 0		
		}
		var expected_b : handler.IResult = {
			type: handler.ResultType.cancel,
			notif : notif_b,
			startQuote: quote_b1,
			quote: quote_b2,
			reason: "cancel buy when current price 110 become bigger than original price 100 by 1 percent",
			expire: null,
			quantity: 0		
		}		
		
		//console.log(res.messages);
				
		expect(res.messages).eqls(
			[
				onNext(300, expected_a),
				onNext(700, expected_b),
				onCompleted(1000)
			]
			);
	});
	
});		