/// <reference path="../typings/tsd.d.ts"/>
/// <reference path="../src/buy-handler.ts"/>
var chai = require('chai');
var handler = require('../src/buy-handler');
var Rx = require('rx/index');
var expect = chai.expect;
var onNext = Rx.ReactiveTest.onNext, onCompleted = Rx.ReactiveTest.onCompleted, subscribe = Rx.ReactiveTest.subscribe;
describe("buy expire", function () {
    it("n1 - eod - expired", function () {
        var notif = {
            key: "0",
            issuer: "test",
            date: (new Date()).toISOString(),
            type: "INotifSilverSurferData",
            data: {
                ticket: "SBER",
                oper: "buy",
                stop: null,
                force: null
            }
        };
        var scheduler = new Rx.TestScheduler();
        var completedTime = 24 * 60 * 60 * 1000;
        var expireTime = 16 * 60 * 60 * 1000;
        var sss = scheduler.createHotObservable(onNext(200, notif), onCompleted(completedTime));
        var qs = scheduler.createHotObservable(onCompleted(completedTime));
        var opts = {
            silverSurferStream: sss,
            quotesStream: qs,
            endOfDay: 16,
            buyPercent: 1,
            cancelBuyPercent: 1
        };
        var res = scheduler.startWithTiming(function () {
            return handler.handleBuy(opts, scheduler);
        }, 0, 0, completedTime);
        var expected = {
            type: handler.ResultType.expired,
            notif: notif,
            startQuote: null,
            quote: null,
            expire: expireTime - 200,
            reason: "Canceled by end of the day",
            quantity: 0
        };
        expect(res.messages).eqls([
            onNext(expireTime + 1, expected),
            onCompleted(completedTime)
        ]);
    });
    it.only("q1 - n1 - eod - expired", function () {
        var completedTime = 24 * 60 * 60 * 1000;
        var expireTime = 16 * 60 * 60 * 1000;
        var notif = {
            key: "0",
            issuer: "test",
            date: (new Date()).toISOString(),
            type: "INotifSilverSurferData",
            data: {
                ticket: "SBER",
                oper: "buy",
                stop: null,
                force: null
            }
        };
        var quote1 = {
            ticket: "SBER",
            latestPrice: 100,
            bid: 100,
            ask: 100
        };
        var scheduler = new Rx.TestScheduler();
        var sss = scheduler.createHotObservable(onNext(200, notif), onCompleted(completedTime));
        var qs = scheduler.createHotObservable(onNext(100, [quote1]), onCompleted(completedTime));
        var opts = {
            silverSurferStream: sss,
            quotesStream: qs,
            endOfDay: 16,
            buyPercent: 1,
            cancelBuyPercent: 1
        };
        var res = scheduler.startWithTiming(function () {
            return handler.handleBuy(opts, scheduler);
        }, 0, 0, completedTime);
        var expected = {
            type: handler.ResultType.expired,
            notif: notif,
            startQuote: quote1,
            quote: null,
            expire: expireTime - 200,
            reason: "Canceled by end of the day",
            quantity: 0
        };
        expect(res.messages).eqls([
            onNext(expireTime, expected),
            onCompleted(completedTime)
        ]);
    });
    it("n1 - n2 - q1 - eod - expired", function () {
        var completedTime = 24 * 60 * 60 * 1000;
        var expireTime = 16 * 60 * 60 * 1000;
        var notif1 = {
            key: "0",
            issuer: "test",
            date: (new Date()).toISOString(),
            type: "INotifSilverSurferData",
            data: {
                ticket: "SBER",
                oper: "buy",
                stop: null,
                force: null
            }
        };
        var notif2 = {
            key: "0",
            issuer: "test",
            date: (new Date()).toISOString(),
            type: "INotifSilverSurferData",
            data: {
                ticket: "HYDR",
                oper: "buy",
                stop: null,
                force: null
            }
        };
        var quote1 = {
            ticket: "SBER",
            latestPrice: 100,
            bid: 100,
            ask: 100
        };
        var scheduler = new Rx.TestScheduler();
        var sss = scheduler.createHotObservable(onNext(200, notif1), onNext(300, notif2), onCompleted(completedTime));
        var qs = scheduler.createHotObservable(onNext(500, [quote1]), onCompleted(completedTime));
        var opts = {
            silverSurferStream: sss,
            quotesStream: qs,
            endOfDay: 16,
            buyPercent: 1,
            cancelBuyPercent: 1
        };
        var res = scheduler.startWithTiming(function () {
            return handler.handleBuy(opts, scheduler);
        }, 0, 0, completedTime);
        var expected = [
            {
                type: handler.ResultType.expired,
                notif: notif1,
                startQuote: quote1,
                quote: null,
                expire: expireTime - 200,
                reason: "Canceled by end of the day",
                quantity: 0
            },
            {
                type: handler.ResultType.expired,
                notif: notif2,
                startQuote: null,
                quote: null,
                expire: expireTime - 200 - 100,
                reason: "Canceled by end of the day",
                quantity: 0
            }
        ];
        expect(res.messages).eqls([
            onNext(expireTime, expected[0]),
            onNext(expireTime + 1, expected[1]),
            onCompleted(completedTime)
        ]);
    });
    it("q1 - n1 - q2 - eod - expired", function () {
        var completedTime = 24 * 60 * 60 * 1000;
        var expireTime = 16 * 60 * 60 * 1000;
        var notif = {
            key: "0",
            issuer: "test",
            date: (new Date()).toISOString(),
            type: "INotifSilverSurferData",
            data: {
                ticket: "SBER",
                oper: "buy",
                stop: null,
                force: null
            }
        };
        var quote1 = {
            ticket: "SBER",
            latestPrice: 100,
            bid: 100,
            ask: 100
        };
        var quote2 = {
            ticket: "SBER",
            latestPrice: 100.1,
            bid: 100.1,
            ask: 100.1
        };
        var scheduler = new Rx.TestScheduler();
        var sss = scheduler.createHotObservable(onNext(200, notif), onCompleted(completedTime));
        var qs = scheduler.createHotObservable(onNext(100, [quote1]), onNext(300, [quote2]), onCompleted(completedTime));
        var opts = {
            silverSurferStream: sss,
            quotesStream: qs,
            endOfDay: 16,
            buyPercent: 1,
            cancelBuyPercent: 1
        };
        var res = scheduler.startWithTiming(function () {
            return handler.handleBuy(opts, scheduler);
        }, 0, 0, completedTime);
        var expected = {
            type: handler.ResultType.expired,
            notif: notif,
            startQuote: quote1,
            quote: null,
            expire: expireTime - 200,
            reason: "Canceled by end of the day",
            quantity: 0
        };
        expect(res.messages).eqls([
            onNext(expireTime, expected),
            onCompleted(completedTime)
        ]);
    });
    it("q1 - n1 - q2 - n2 - eod - expired", function () {
        var completedTime = 24 * 60 * 60 * 1000;
        var expireTime = 16 * 60 * 60 * 1000;
        var notif1 = {
            key: "0",
            issuer: "test",
            date: (new Date()).toISOString(),
            type: "INotifSilverSurferData",
            data: {
                ticket: "SBER",
                oper: "buy",
                stop: null,
                force: null
            }
        };
        var notif2 = {
            key: "0",
            issuer: "test",
            date: (new Date()).toISOString(),
            type: "INotifSilverSurferData",
            data: {
                ticket: "SBER",
                oper: "buy",
                stop: null,
                force: null
            }
        };
        var quote1 = {
            ticket: "SBER",
            latestPrice: 100,
            bid: 100,
            ask: 100
        };
        var quote2 = {
            ticket: "SBER",
            latestPrice: 100.1,
            bid: 100.1,
            ask: 100.1
        };
        var scheduler = new Rx.TestScheduler();
        var sss = scheduler.createHotObservable(onNext(200, notif1), onNext(400, notif2), onCompleted(completedTime));
        var qs = scheduler.createHotObservable(onNext(100, [quote1]), onNext(300, [quote2]), onCompleted(completedTime));
        var opts = {
            silverSurferStream: sss,
            quotesStream: qs,
            endOfDay: 16,
            buyPercent: 1,
            cancelBuyPercent: 1
        };
        var res = scheduler.startWithTiming(function () {
            return handler.handleBuy(opts, scheduler);
        }, 0, 0, completedTime);
        var expected = [
            {
                type: handler.ResultType.expired,
                notif: notif1,
                startQuote: quote1,
                quote: null,
                expire: expireTime - 200,
                reason: "Canceled by end of the day",
                quantity: 0
            },
            {
                type: handler.ResultType.expired,
                notif: notif2,
                startQuote: quote2,
                quote: null,
                expire: expireTime - 200,
                reason: "Canceled by end of the day",
                quantity: 0
            }];
        expect(res.messages).eqls([
            onNext(expireTime, expected[0]),
            onNext(expireTime, expected[1]),
            onCompleted(completedTime)
        ]);
    });
    it("q1 - n1 - eod - q2 - expired", function () {
        var completedTime = 24 * 60 * 60 * 1000;
        var expireTime = 16 * 60 * 60 * 1000;
        var notif = {
            key: "0",
            issuer: "test",
            date: (new Date()).toISOString(),
            type: "INotifSilverSurferData",
            data: {
                ticket: "SBER",
                oper: "buy",
                stop: null,
                force: null
            }
        };
        var quote1 = {
            ticket: "SBER",
            latestPrice: 100,
            bid: 100,
            ask: 100
        };
        var quote2 = {
            ticket: "SBER",
            latestPrice: 120,
            bid: 120,
            ask: 120
        };
        var scheduler = new Rx.TestScheduler();
        var sss = scheduler.createHotObservable(onNext(200, notif), onCompleted(completedTime));
        var qs = scheduler.createHotObservable(onNext(100, [quote1]), onNext(expireTime + 300, [quote2]), onCompleted(completedTime));
        var opts = {
            silverSurferStream: sss,
            quotesStream: qs,
            endOfDay: 16,
            buyPercent: 1,
            cancelBuyPercent: 1
        };
        var res = scheduler.startWithTiming(function () {
            return handler.handleBuy(opts, scheduler);
        }, 0, 0, completedTime);
        var expected = {
            type: handler.ResultType.expired,
            notif: notif,
            startQuote: quote1,
            quote: null,
            expire: expireTime - 200,
            reason: "Canceled by end of the day",
            quantity: 0
        };
        expect(res.messages).eqls([
            onNext(expireTime, expected),
            onCompleted(completedTime)
        ]);
    });
});
