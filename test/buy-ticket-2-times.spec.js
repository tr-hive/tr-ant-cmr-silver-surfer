/// <reference path="../typings/tsd.d.ts"/>
/// <reference path="../src/buy-handler.ts"/>
var chai = require('chai');
var handler = require('../src/buy-handler');
var Rx = require('rx/index');
var expect = chai.expect;
var onNext = Rx.ReactiveTest.onNext, onCompleted = Rx.ReactiveTest.onCompleted, subscribe = Rx.ReactiveTest.subscribe;
describe("buy ticket 2 times", function () {
    it("q1 - n1 - q2 - n2 - q3 - buy first cancel second", function () {
        var notif = {
            key: "0",
            issuer: "test",
            date: (new Date()).toISOString(),
            type: "INotifSilverSurferData",
            data: {
                ticket: "SBER",
                oper: "buy",
                stop: null,
                force: null
            }
        };
        var quote1 = {
            ticket: "SBER",
            latestPrice: 100,
            bid: 100,
            ask: 100
        };
        var quote2 = {
            ticket: "SBER",
            latestPrice: 90,
            bid: 90,
            ask: 90
        };
        var quote3 = {
            ticket: "SBER",
            latestPrice: 110,
            bid: 110,
            ask: 110
        };
        var scheduler = new Rx.TestScheduler();
        var sss = scheduler.createHotObservable(onNext(200, notif), onNext(400, notif), onCompleted(1000));
        var qs = scheduler.createHotObservable(onNext(100, [quote1]), onNext(300, [quote2]), onNext(500, [quote3]), onCompleted(1000));
        var opts = {
            silverSurferStream: sss,
            quotesStream: qs,
            endOfDay: 16,
            buyPercent: 1,
            cancelBuyPercent: 1
        };
        var res = scheduler.startWithTiming(function () {
            return handler.handleBuy(opts, scheduler);
        }, 0, 0, 1200);
        var expected_a = {
            type: handler.ResultType.buy,
            notif: notif,
            startQuote: quote1,
            quote: quote2,
            reason: "buy when original price 100 drops by 1 percent; current price is 90",
            expire: null,
            quantity: 10
        };
        var expected_b = {
            type: handler.ResultType.cancel,
            notif: notif,
            startQuote: quote2,
            quote: quote3,
            reason: "cancel buy when current price 110 become bigger than original price 90 by 1 percent",
            expire: null,
            quantity: 0
        };
        expect(res.messages).eqls([
            onNext(300, expected_a),
            onNext(500, expected_b),
            onCompleted(1000)
        ]);
    });
});
