/// <reference path="../typings/tsd.d.ts"/>
/// <reference path="../src/buy-handler.ts"/>

import * as chai from 'chai'
import * as handler from '../src/sell-handler'
import * as Rx from 'rx/index'
import * as utils from '../node_modules/tr-ant-utils'

var expect = chai.expect;

var onNext = Rx.ReactiveTest.onNext,
    onCompleted = Rx.ReactiveTest.onCompleted,
    subscribe = Rx.ReactiveTest.subscribe;

describe("simple sell",  () => {

	it("n1 - q1 - sell (gain)",  () => {

		var notif: utils.INotif<utils.INotifPortfolioChanged> = {
			key : "1",
			issuer: "test",
			date: '2015-08-29T12:02:22.279Z',
			type: "INotifPortfolioChanged",
			data : {
				portfolio: "test",
				value: 10000,
				cmd: null,
				positions: [
					{
						ticket: "SBER",
						quantity: 100,
						value: 100					
					}
				]		
			}						
		} 
		
		var quote1 = {
				ticket: "SBER",
				latestPrice: 110,
				bid: 110,
				ask: 110				
			};

		var scheduler = new Rx.TestScheduler();

		var ps = scheduler.createHotObservable<utils.INotif<utils.INotifPortfolioChanged>>(
			onNext(200, notif),
			onCompleted(700)
			);


		var qs = scheduler.createHotObservable<utils.IQuote[]>(
			onNext(300, [quote1]),			
			onCompleted(700)
		);
			
		var opts : handler.ISellOpts = {
			portfolioStream: ps, 
			quotesStream: qs,
			sellPercentDown: 1,
			sellPercentUp: 1			
		};		

		var res = scheduler.startWithTiming(() =>
			handler.handleSell(opts, scheduler)
		, 0, 0, 1000);
		
		var expected : handler.IResult = {
			notif : notif,
			quote: quote1,
			position: notif.data.positions[0],
			reason: "Current price 110 bigger than buy price 100 by more than 1 percent (gain)"		
		}
		
		expect(res.messages).eqls(
			[
				onNext(300, expected),
				onCompleted(700)
			]
			);
	});

	it("n1 - q1 - sell (gain) - q2 - nothing",  () => {

		var notif: utils.INotif<utils.INotifPortfolioChanged> = {
			key : "1",
			issuer: "test",
			date: '2015-08-29T12:02:22.279Z',
			type: "INotifPortfolioChanged",
			data : {
				portfolio: "test",
				value: 10000,
				cmd: null,
				positions: [
					{
						ticket: "SBER",
						quantity: 100,
						value: 100					
					}
				]		
			}						
		} 
		
		var quote1 = {
				ticket: "SBER",
				latestPrice: 110,
				bid: 110,
				ask: 110				
			};

		var quote2 = {
				ticket: "SBER",
				latestPrice: 120,
				bid: 120,
				ask: 120				
			};

		var scheduler = new Rx.TestScheduler();

		var ps = scheduler.createHotObservable<utils.INotif<utils.INotifPortfolioChanged>>(
			onNext(200, notif),
			onNext(400, notif),
			onCompleted(700)
			);


		var qs = scheduler.createHotObservable<utils.IQuote[]>(
			onNext(300, [quote1]),			
			onCompleted(700)
		);
			
		var opts : handler.ISellOpts = {
			portfolioStream: ps, 
			quotesStream: qs,
			sellPercentDown: 1,
			sellPercentUp: 1			
		};		

		var res = scheduler.startWithTiming(() =>
			handler.handleSell(opts, scheduler)
		, 0, 0, 1000);
		
		var expected : handler.IResult = {
			notif : notif,
			quote: quote1,
			position: notif.data.positions[0],
			reason: "Current price 110 bigger than buy price 100 by more than 1 percent (gain)"		
		}
		
		expect(res.messages).eqls(
			[
				onNext(300, expected),
				onCompleted(700)
			]
			);
	});


	it("n1 - q1 - sell (loss)",  () => {

		var notif: utils.INotif<utils.INotifPortfolioChanged> = {
			key : "1",
			issuer: "test",
			date: '2015-08-29T12:02:22.279Z',
			type: "INotifPortfolioChanged",
			data : {
				portfolio: "test",
				value: 10000,
				cmd: null,
				positions: [
					{
						ticket: "SBER",
						quantity: 100,
						value: 100					
					}
				]		
			}						
		} 
		
		var quote1 = {
				ticket: "SBER",
				latestPrice: 90,
				bid: 90,
				ask: 90				
			};

		var scheduler = new Rx.TestScheduler();

		var ps = scheduler.createHotObservable<utils.INotif<utils.INotifPortfolioChanged>>(
			onNext(200, notif),
			onCompleted(700)
			);


		var qs = scheduler.createHotObservable<utils.IQuote[]>(
			onNext(300, [quote1]),			
			onCompleted(700)
		);
			
		var opts : handler.ISellOpts = {
			portfolioStream: ps, 
			quotesStream: qs,
			sellPercentDown: 1,
			sellPercentUp: 1			
		};		

		var res = scheduler.startWithTiming(() =>
			handler.handleSell(opts, scheduler)
		, 0, 0, 1000);
		
		var expected : handler.IResult = {
			notif : notif,
			quote: quote1,
			position: notif.data.positions[0],
			reason: "Current price 90 less than buy price 100 by more than 1 percent (loss)"		
		}
		
		expect(res.messages).eqls(
			[
				onNext(300, expected),
				onCompleted(700)
			]
			);
	});

	it("n1 - q1 - q2 - no sell condition",  () => {

		var notif: utils.INotif<utils.INotifPortfolioChanged> = {
			key : "1",
			issuer: "test",
			date: '2015-08-29T12:02:22.279Z',
			type: "INotifPortfolioChanged",
			data : {
				portfolio: "test",
				value: 10000,
				cmd: null,
				positions: [
					{
						ticket: "SBER",
						quantity: 100,
						value: 100					
					}
				]		
			}						
		} 
		
		var quote1 = {
				ticket: "SBER",
				latestPrice: 100.1,
				bid: 101,
				ask: 101				
			};
			
		var quote2 = {
				ticket: "SBER",
				latestPrice: 99.9,
				bid: 99,
				ask: 99				
			};
			
		var scheduler = new Rx.TestScheduler();

		var ps = scheduler.createHotObservable<utils.INotif<utils.INotifPortfolioChanged>>(
			onNext(200, notif),
			onCompleted(700)
			);


		var qs = scheduler.createHotObservable<utils.IQuote[]>(
			onNext(300, [quote1]),			
			onNext(400, [quote2]),
			onCompleted(700)
		);
			
		var opts : handler.ISellOpts = {
			portfolioStream: ps, 
			quotesStream: qs,
			sellPercentDown: 1,
			sellPercentUp: 1			
		};		

		var res = scheduler.startWithTiming(() =>
			handler.handleSell(opts, scheduler)
		, 0, 0, 1000);
		
		expect(res.messages).eqls(
			[
				onCompleted(700)
			]
			);
	});
			
});		