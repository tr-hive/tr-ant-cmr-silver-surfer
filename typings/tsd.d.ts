/// <reference path="rabbit.js/rabbit.js.d.ts" />
/// <reference path="bluebird/bluebird.d.ts" />
/// <reference path="mocha/mocha.d.ts" />
/// <reference path="chai/chai.d.ts" />
/// <reference path="sinon-chai/sinon-chai.d.ts" />
/// <reference path="sinon/sinon.d.ts" />
/// <reference path="moment/moment.d.ts" />
/// <reference path="moment/moment-node.d.ts" />

/// <reference path="../node_modules/rx/ts/rx.all.d.ts" />
/// <reference path="rx.testing.d.ts" />
/// <reference path="../node_modules/tr-ant-utils/dist/utils.d.ts" />
/// <reference path="../node_modules/tr-ant-utils/dist/types.d.ts" />
/// <reference path="../node_modules/da-combinator-rx/dist/combinator.d.ts" />
/// <reference path="node/node.d.ts" />

declare module "rx/index" {
	import index = require("rx");
	export = index;
}
