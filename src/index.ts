 /// <reference path="../typings/tsd.d.ts" />
 
import * as rabbit from "da-rabbitmq-rx"
import * as logs from "da-logs"
import * as moment from "moment"
import * as Rx from "rx";
import * as utils from "tr-ant-utils"
import * as buyHandler from "./buy-handler"
import * as sellHandler from "./sell-handler"
var uuid = require("node-uuid")
 
import getEnvVar = utils.getEnvVar;

const PKG_NAME = require("../package.json").name;

const RABBIT_URI = getEnvVar("RABBIT_URI");
const RABBIT_QUEUE_CMDS = getEnvVar("RABBIT_QUEUE_COMMANDS");
const RABBIT_QUEUE_NOTIFS = getEnvVar("RABBIT_QUEUE_NOTIFS");
const RABBIT_QUEUE_REQUEST_NOTIF = getEnvVar("RABBIT_QUEUE_REQUEST_NOTIF");
const RABBIT_QUEUE_QUOTES = getEnvVar("RABBIT_QUEUE_QUOTES");
const BUY_PERCENT = parseFloat(getEnvVar("CMR_SILVER_SURFER_BUY_PERCENT"));
const CANCEL_BUY_PERCENT = parseFloat(getEnvVar("CMR_SILVER_SURFER_CANCEL_BUY_PERCENT"));
const END_OF_DAY = parseInt(getEnvVar("CMR_SILVER_SURFER_BUY_END_OF_DAY"));
const SELL_PERCENT_UP = parseFloat(getEnvVar("CMR_SILVER_SURFER_SELL_PERCENT_UP"));
const SELL_PERCENT_DOWN = parseFloat(getEnvVar("SCMR_SILVER_SURFER_ELL_PERCENT_DOWN"));
const LOG_LOGGLY_KEY = getEnvVar("LOG_LOGGLY_KEY");
const LOG_LOGGLY_SUBDOMAIN = getEnvVar("LOG_LOGGLY_SUBDOMAIN");
const LOG_MONGO_URI = getEnvVar("LOG_MONGO_URI");
const LOG_MONGO_COLLECTION = getEnvVar("LOG_MONGO_COLLECTION");
 
var logger = new logs.LoggerCompose({pack : <any>require("../package.json"), tags : ["commander"]},  {
    loggly: {token: LOG_LOGGLY_KEY, subdomain: LOG_LOGGLY_SUBDOMAIN},
    mongo: {connection: LOG_MONGO_URI, collection: LOG_MONGO_COLLECTION},
    console: true
 });
 
 logger.write({resource: "module", status : "starting", opts: {
	RABBIT_URI: RABBIT_URI, 
	RABBIT_QUEUE_CMDS: RABBIT_QUEUE_CMDS,
	RABBIT_QUEUE_NOTIFS: RABBIT_QUEUE_NOTIFS,
	RABBIT_QUEUE_REQUEST_NOTIF: RABBIT_QUEUE_REQUEST_NOTIF,
	RABBIT_QUEUE_QUOTES: RABBIT_QUEUE_QUOTES,
	BUY_PERCENT: BUY_PERCENT,
	CANCEL_BUY_PERCENT: CANCEL_BUY_PERCENT,
	END_OF_DAY: END_OF_DAY,
	SELL_PERCENT_UP: SELL_PERCENT_UP,
	SELL_PERCENT_DOWN: SELL_PERCENT_DOWN,
	LOG_LOGGLY_KEY: LOG_LOGGLY_KEY,
	LOG_LOGGLY_SUBDOMAIN: LOG_LOGGLY_SUBDOMAIN,
	LOG_MONGO_URI: LOG_MONGO_URI,
	LOG_MONGO_COLLECTION: LOG_MONGO_COLLECTION 	 
}});

var pubOpts = {uri: RABBIT_URI, socketType: rabbit.SocketType.PUB, queue: RABBIT_QUEUE_CMDS};
var pub = new rabbit.RabbitPub(pubOpts); 
pub.connect();
pub.connectStream.subscribe(() =>
 logger.write({resource: "rabbit", oper: "connected", status : "success", opts: pubOpts}) 
, (err) => {
	logger.write({resource: "rabbit", status : "error", err: err, opts: pubOpts});
	process.exit(1);
});

var notifRequestOpts = {uri: RABBIT_URI, socketType: rabbit.SocketType.PUB, queue: RABBIT_QUEUE_REQUEST_NOTIF};
var pubNotifRequest = new rabbit.RabbitPub(notifRequestOpts); 
pubNotifRequest.connect();
pubNotifRequest.connectStream.subscribe(() =>
 logger.write({resource: "rabbit", oper: "connected", status : "success", opts: notifRequestOpts}) 
, (err) => {
	logger.write({resource: "rabbit", status : "error", err: err, opts: notifRequestOpts});
	process.exit(1);
});

var quotesSubOpts = {uri: RABBIT_URI, socketType: rabbit.SocketType.SUB, queue: RABBIT_QUEUE_QUOTES};
var quotesSub = new rabbit.RabbitSub(quotesSubOpts); 
quotesSub.connect();
quotesSub.stream.take(1).subscribe(() =>
 logger.write({resource: "rabbit", oper: "connected", status : "success", opts: quotesSubOpts}) 
, (err) => {
	logger.write({resource: "rabbit", oper: "connected", status : "error", err: err, opts: quotesSubOpts});
	process.exit(1);
});

var notifsSubOpts = {uri: RABBIT_URI, socketType: rabbit.SocketType.SUB, queue: RABBIT_QUEUE_NOTIFS};
var notifsSub = new rabbit.RabbitSub(notifsSubOpts); 
notifsSub.connect();
notifsSub.stream.take(1).subscribe(() => {
	pubNotifRequest.write(<utils.INotifRequest<utils.INotifRequestPortfolioQuery>>
		{type: "INotifPortfolioChanged", query : {portfolio : PKG_NAME}});		
	logger.write({resource: "rabbit", oper: "connected", status : "success", opts: notifsSubOpts});
}, (err) => {
	logger.write({resource: "rabbit", oper: "connected", status : "error", err: err, opts: notifsSubOpts});
	process.exit(1);
});

var quotesStream = quotesSub.stream.skip(1).share();

var buyOpts : buyHandler.IBuyOpts = {
	silverSurferStream: notifsSub.stream.skip(1)
		.filter(f => f.type == "INotifSilverSurferData").share(),
	quotesStream: quotesStream,
	endOfDay: END_OF_DAY,
	buyPercent: BUY_PERCENT,
	cancelBuyPercent: CANCEL_BUY_PERCENT
};		

var sellOpts : sellHandler.ISellOpts = {
	portfolioStream: notifsSub.stream.skip(1)
		.filter(f => f.type == "INotifPortfolioChanged").share(),
	quotesStream: quotesStream,
	sellPercentUp: SELL_PERCENT_UP,
	sellPercentDown: SELL_PERCENT_DOWN,	
};

var buys = buyHandler.handleBuy(buyOpts);
var sells = sellHandler.handleSell(sellOpts);

Rx.Observable.merge(
	<any>buyOpts.silverSurferStream,
	<any>sellOpts.portfolioStream
).subscribe(val =>
	logger.write({resource: "handle", status : "start", group : "notif", data : val})	
, (err) => 
	logger.write({resource: "handle", status : "error", group : "notif", err : err}));
	
Rx.Observable.merge(
	<any>buyHandler.handleBuy(buyOpts),
	<any>sellHandler.handleSell(sellOpts)
).subscribe(val =>
	logger.write({resource: "handle", status : "success", group : "notif", data : val})	
, (err) => 
	logger.write({resource: "handle", status : "error", group : "notif", err : err}));
	

buys
.filter(f => f.type == buyHandler.ResultType.buy)
.map<utils.ICmd>(val => ({
	key : uuid.v4(),
	issuer : PKG_NAME,
	date: (new Date()).toISOString(),
	reason: val.reason,
	notif: val.notif,
	portfolio: PKG_NAME,
		trade : {
		ticket: val.notif.data.ticket,
		quantity: val.quantity,
		oper: "buy",
	}	
}))
.subscribe(val => pub.write(val));

sells.map<utils.ICmd>(val => ({
	key : `[${val.notif.key}]_[${val.position.ticket}]_${PKG_NAME}`,
	issuer : PKG_NAME,
	date: (new Date()).toISOString(),
	reason: val.reason,
	notif: val.notif,
	portfolio: PKG_NAME,
		trade : {
		ticket: val.position.ticket,
		quantity: val.position.quantity,
		oper: "sell",
	}			
})).subscribe(val => pub.write(val));