/// <reference path="../typings/tsd.d.ts" />
import * as Rx from "rx"
import * as combinator from "da-combinator-rx"
import * as utils from "tr-ant-utils"
import * as moment from "moment"


export interface ISellOpts {  
	portfolioStream: Rx.Observable<utils.INotif<utils.INotifPortfolioChanged>>
	quotesStream: Rx.Observable<utils.IQuote[]>
	sellPercentUp: number
	sellPercentDown: number	
} 

function shouldSell(buyPrice: number, currentPrice: number, sellPercentUp: number, sellPercentDown: number): string { 
  if (currentPrice <= buyPrice * (1 - sellPercentDown / 100)) {
    return `Current price ${currentPrice} less than buy price ${buyPrice} by more than ${sellPercentDown} percent (loss)`;
  }
  else if (currentPrice >= buyPrice * (1 + sellPercentUp / 100)) {
    return `Current price ${currentPrice} bigger than buy price ${buyPrice} by more than ${sellPercentUp} percent (gain)`;
  }
  return null;
}

export interface IResult {
  quote : utils.IQuote,
  notif : utils.INotif<utils.INotifPortfolioChanged>,
  position : utils.IPortfolioPosition,
  reason: string
}

export function handleSell(opts: ISellOpts, scheduler?: Rx.IScheduler) : Rx.Observable<IResult> {
  
  	var quotes = opts.quotesStream.selectMany(p => p);
    //notif + positions 
    var positionsStream = opts.portfolioStream.map(val => ({notif : val, poss : val.data.positions.filter(f => f.quantity > 0)}))
    .shareReplay(null, 1, scheduler); //ensure wait quote if position arrived first
    
  	//quote & [notif + positions] => [quote + notif + position]
  	var qps = quotes.withLatestFrom(positionsStream, (quote, tlp) => 
      ({ quote : quote, notif : tlp.notif, pos : tlp.poss.filter(f => f.ticket == quote.ticket)[0]})) //find position by ticket
      .filter(tlp => !!tlp.pos); // quote but no pos
      

    return qps.map(tlp =>
      ({data : tlp, reason : shouldSell(tlp.pos.value, tlp.quote.latestPrice, opts.sellPercentUp, opts.sellPercentDown)}))
      .filter(v => !!v.reason) //filter of no reason to sell       
      .map(tlp => ({
        quote : tlp.data.quote,
        notif : tlp.data.notif,
        position : tlp.data.pos,
        reason: tlp.reason        
      })).share();                       
}    



