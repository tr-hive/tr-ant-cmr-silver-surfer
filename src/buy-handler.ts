/// <reference path="../typings/tsd.d.ts" />

import * as Rx from "rx"
import * as combinator from "da-combinator-rx"
import * as utils from "tr-ant-utils"
import * as moment from "moment"

type IQuote = utils.IQuote;
type ISS = utils.INotif<utils.INotifSilverSurferData>


interface IEOD {
	notif: ISS, expire : number
}

export interface IBuyOpts {
	silverSurferStream: Rx.Observable<utils.INotif<utils.INotifSilverSurferData>>,
	quotesStream: Rx.Observable<utils.IQuote[]>
    //utc hour
    endOfDay: number
	buyPercent: number
	cancelBuyPercent: number
}

function getMillisecondsFromStartOfTheDay() {
	var now = moment.utc();
	return now.hours() * 60 * 60 * 1000 +
	now.minutes() * 60 * 1000 + 
	now.seconds() * 1000 + 
	now.milliseconds(); 
}

function getTillEndOfDay(eod: number, scheduler: Rx.IScheduler): number {
	 
	var _now = scheduler ? 
		moment.utc().startOf("d").add((<any>scheduler).now(), "ms") :
		moment.utc();
	
	var _eod = moment.utc();
	_eod.add("d", _now.hours() < eod ? 0 : 1).startOf("d").add(eod, "h");
	
	var diff = _eod.diff(_now, "ms");
								 
	return diff;
}

function getEODTicket(qeod: {quote : IQuote, eod : IEOD}) {
	return qeod.quote ? qeod.quote.ticket : qeod.eod.notif.data.ticket;	
}

function createEndOfDayStream(notifs: Rx.Observable<ISS>, endOfDay: number, scheduler: Rx.IScheduler): Rx.Observable<IEOD> {
	return notifs.selectMany(n => {
		var till = getTillEndOfDay(endOfDay, scheduler);
		return Rx.Observable.timer(till, scheduler).map(m => ({notif : n, expire : till}))
	});
}

export enum ResultType { buy, cancel, expired }

export interface IResult {
	type: ResultType
	notif: utils.INotif<utils.INotifSilverSurferData>
	startQuote: utils.IQuote
	quote: utils.IQuote
	reason: string
	expire: number
	quantity: number
}

export function handleBuy(opts: IBuyOpts, scheduler?: Rx.IScheduler): Rx.Observable<IResult> {

	var sss = opts.silverSurferStream.filter(f => f.data.oper == "buy");
	var quotes = opts.quotesStream.selectMany(p => p);
	var eods = createEndOfDayStream(opts.silverSurferStream, opts.endOfDay, scheduler);

	var quotes_eods = quotes
	.map(m => ({quote : m, eod : null})) //just quote
	.merge(eods.map(m => ({quote : null, eod : m}))) //end of days
	.share();	
	
	//Notif & [Quote + EOD] => [Notif + (Quote | EOD)]
	var nqes = combinator.combineGroup(sss, quotes_eods, v => {
		if (v.type == combinator.StreamType.primary) {
			//notif
			return (<ISS>v.item).data.ticket;
		}
		else {
			//quote | expire
			return getEODTicket(v.item);
		}
	}, scheduler)
	.map(m => ({
		notif: m.p,
		expire : m.s.eod ? <number>m.s.eod.expire : null,
		quote : m.s.quote 
	})).share();
	
	//nqes.subscribe(utils.createObserver("nqes"));
							
	var eod_nqs = nqes.filter(f => !!f.expire).map<IResult>(v => (
		{
			notif: v.notif,
			quote: null,
			startQuote: v.quote,
			reason: "Canceled by end of the day",
			type: ResultType.expired,
			expire: v.expire,
			quantity: 0
		}
		));
			
	//proceed_nqs.subscribe(utils.createObserver("proceed_nqs"));
	//quotes_eods.subscribe(utils.createObserver("quotes_eods"));

	//[Notif + Start Quote] & [Quote + EOD] => [Notif + Start Quote + (Quote | EOD)]
	//here problem
	var nqqs = nqes.flatMap(x => {
		//[v] could contain or quote or expired
		console.log("+++", JSON.stringify(x, null, 2));
		return quotes_eods.filter(f => getEODTicket(f) == x.notif.data.ticket).map(q => ({
			notif: x.notif,
			startQuote: x.quote,
			quote: q.quote,
			expire: q.eod ? <number>q.eod.expire : null
		}))
		.map<IResult>(v => {
				console.log("---", JSON.stringify(v, null, 2));
				var res: { reason: string, type: ResultType };			
				if (v.expire) {
					res = { reason: "Canceled by end of the day", type: ResultType.expired };
				}
				else if (v.quote.latestPrice <= v.startQuote.latestPrice * (1 - opts.buyPercent / 100)) {
					res = {
						reason: `buy when original price ${v.startQuote.latestPrice} drops by ${opts.buyPercent} percent; current price is ${v.quote.latestPrice}`,
						type: ResultType.buy
					}
				}
				else if (v.quote.latestPrice >= v.startQuote.latestPrice * (1 + opts.cancelBuyPercent / 100)) {
					res = {
						reason: `cancel buy when current price ${v.quote.latestPrice} become bigger than original price ${v.startQuote.latestPrice} by ${opts.cancelBuyPercent} percent`,
						type: ResultType.cancel
					}
				}

				if (res) {
					return {
						notif: v.notif,
						startQuote: v.startQuote,
						quote: v.quote,
						reason: res.reason,
						type: res.type,
						expire: v.expire,
						quantity: res.type == ResultType.buy ? utils.getSecurity(v.quote.ticket).lotSize : 0
					}
				}
				else {
					return null;
				}
			})
			.skipWhile(v => !v)
			.take(1)
	}).share();

	//nqqs.subscribe(utils.createObserver("nqqs"));
		
	//
	
	//buyStream.subscribe(utils.createObserver("buyStream"));
	//cancelBuyStream.subscribe(utils.createObserver("cancelBuyStream"));
	//eofStream.subscribe(utils.createObserver("eofStream"));
		 		
	return nqqs.merge(eod_nqs).share();
}
