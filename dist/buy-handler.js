/// <reference path="../typings/tsd.d.ts" />
var Rx = require("rx");
var combinator = require("da-combinator-rx");
var utils = require("tr-ant-utils");
var moment = require("moment");
function getMillisecondsFromStartOfTheDay() {
    var now = moment.utc();
    return now.hours() * 60 * 60 * 1000 +
        now.minutes() * 60 * 1000 +
        now.seconds() * 1000 +
        now.milliseconds();
}
function getTillEndOfDay(eod, scheduler) {
    var _now = scheduler ?
        moment.utc().startOf("d").add(scheduler.now(), "ms") :
        moment.utc();
    var _eod = moment.utc();
    _eod.add("d", _now.hours() < eod ? 0 : 1).startOf("d").add(eod, "h");
    var diff = _eod.diff(_now, "ms");
    return diff;
}
function getEODTicket(qeod) {
    return qeod.quote ? qeod.quote.ticket : qeod.eod.notif.data.ticket;
}
function createEndOfDayStream(notifs, endOfDay, scheduler) {
    return notifs.selectMany(function (n) {
        var till = getTillEndOfDay(endOfDay, scheduler);
        return Rx.Observable.timer(till, scheduler).map(function (m) { return ({ notif: n, expire: till }); });
    });
}
(function (ResultType) {
    ResultType[ResultType["buy"] = 0] = "buy";
    ResultType[ResultType["cancel"] = 1] = "cancel";
    ResultType[ResultType["expired"] = 2] = "expired";
})(exports.ResultType || (exports.ResultType = {}));
var ResultType = exports.ResultType;
function handleBuy(opts, scheduler) {
    var sss = opts.silverSurferStream.filter(function (f) { return f.data.oper == "buy"; });
    var quotes = opts.quotesStream.selectMany(function (p) { return p; });
    var eods = createEndOfDayStream(opts.silverSurferStream, opts.endOfDay, scheduler);
    var quotes_eods = quotes
        .map(function (m) { return ({ quote: m, eod: null }); })
        .merge(eods.map(function (m) { return ({ quote: null, eod: m }); }))
        .share();
    var nqes = combinator.combineGroup(sss, quotes_eods, function (v) {
        if (v.type == combinator.StreamType.primary) {
            return v.item.data.ticket;
        }
        else {
            return getEODTicket(v.item);
        }
    }, scheduler)
        .map(function (m) { return ({
        notif: m.p,
        expire: m.s.eod ? m.s.eod.expire : null,
        quote: m.s.quote
    }); }).share();
    var eod_nqs = nqes.filter(function (f) { return !!f.expire; }).map(function (v) { return ({
        notif: v.notif,
        quote: null,
        startQuote: v.quote,
        reason: "Canceled by end of the day",
        type: ResultType.expired,
        expire: v.expire,
        quantity: 0
    }); });
    var nqqs = nqes.flatMap(function (x) {
        console.log("+++", JSON.stringify(x, null, 2));
        return quotes_eods.filter(function (f) { return getEODTicket(f) == x.notif.data.ticket; }).map(function (q) { return ({
            notif: x.notif,
            startQuote: x.quote,
            quote: q.quote,
            expire: q.eod ? q.eod.expire : null
        }); })
            .map(function (v) {
            console.log("---", JSON.stringify(v, null, 2));
            var res;
            if (v.expire) {
                res = { reason: "Canceled by end of the day", type: ResultType.expired };
            }
            else if (v.quote.latestPrice <= v.startQuote.latestPrice * (1 - opts.buyPercent / 100)) {
                res = {
                    reason: "buy when original price " + v.startQuote.latestPrice + " drops by " + opts.buyPercent + " percent; current price is " + v.quote.latestPrice,
                    type: ResultType.buy
                };
            }
            else if (v.quote.latestPrice >= v.startQuote.latestPrice * (1 + opts.cancelBuyPercent / 100)) {
                res = {
                    reason: "cancel buy when current price " + v.quote.latestPrice + " become bigger than original price " + v.startQuote.latestPrice + " by " + opts.cancelBuyPercent + " percent",
                    type: ResultType.cancel
                };
            }
            if (res) {
                return {
                    notif: v.notif,
                    startQuote: v.startQuote,
                    quote: v.quote,
                    reason: res.reason,
                    type: res.type,
                    expire: v.expire,
                    quantity: res.type == ResultType.buy ? utils.getSecurity(v.quote.ticket).lotSize : 0
                };
            }
            else {
                return null;
            }
        })
            .skipWhile(function (v) { return !v; })
            .take(1);
    }).share();
    return nqqs.merge(eod_nqs).share();
}
exports.handleBuy = handleBuy;
