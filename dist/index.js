/// <reference path="../typings/tsd.d.ts" />
var rabbit = require("da-rabbitmq-rx");
var logs = require("da-logs");
var Rx = require("rx");
var utils = require("tr-ant-utils");
var buyHandler = require("./buy-handler");
var sellHandler = require("./sell-handler");
var uuid = require("node-uuid");
var getEnvVar = utils.getEnvVar;
var PKG_NAME = require("../package.json").name;
var RABBIT_URI = getEnvVar("RABBIT_URI");
var RABBIT_QUEUE_CMDS = getEnvVar("RABBIT_QUEUE_COMMANDS");
var RABBIT_QUEUE_NOTIFS = getEnvVar("RABBIT_QUEUE_NOTIFS");
var RABBIT_QUEUE_REQUEST_NOTIF = getEnvVar("RABBIT_QUEUE_REQUEST_NOTIF");
var RABBIT_QUEUE_QUOTES = getEnvVar("RABBIT_QUEUE_QUOTES");
var BUY_PERCENT = parseFloat(getEnvVar("CMR_SILVER_SURFER_BUY_PERCENT"));
var CANCEL_BUY_PERCENT = parseFloat(getEnvVar("CMR_SILVER_SURFER_CANCEL_BUY_PERCENT"));
var END_OF_DAY = parseInt(getEnvVar("CMR_SILVER_SURFER_BUY_END_OF_DAY"));
var SELL_PERCENT_UP = parseFloat(getEnvVar("CMR_SILVER_SURFER_SELL_PERCENT_UP"));
var SELL_PERCENT_DOWN = parseFloat(getEnvVar("SCMR_SILVER_SURFER_ELL_PERCENT_DOWN"));
var LOG_LOGGLY_KEY = getEnvVar("LOG_LOGGLY_KEY");
var LOG_LOGGLY_SUBDOMAIN = getEnvVar("LOG_LOGGLY_SUBDOMAIN");
var LOG_MONGO_URI = getEnvVar("LOG_MONGO_URI");
var LOG_MONGO_COLLECTION = getEnvVar("LOG_MONGO_COLLECTION");
var logger = new logs.LoggerCompose({ pack: require("../package.json"), tags: ["commander"] }, {
    loggly: { token: LOG_LOGGLY_KEY, subdomain: LOG_LOGGLY_SUBDOMAIN },
    mongo: { connection: LOG_MONGO_URI, collection: LOG_MONGO_COLLECTION },
    console: true
});
logger.write({ resource: "module", status: "starting", opts: {
        RABBIT_URI: RABBIT_URI,
        RABBIT_QUEUE_CMDS: RABBIT_QUEUE_CMDS,
        RABBIT_QUEUE_NOTIFS: RABBIT_QUEUE_NOTIFS,
        RABBIT_QUEUE_REQUEST_NOTIF: RABBIT_QUEUE_REQUEST_NOTIF,
        RABBIT_QUEUE_QUOTES: RABBIT_QUEUE_QUOTES,
        BUY_PERCENT: BUY_PERCENT,
        CANCEL_BUY_PERCENT: CANCEL_BUY_PERCENT,
        END_OF_DAY: END_OF_DAY,
        SELL_PERCENT_UP: SELL_PERCENT_UP,
        SELL_PERCENT_DOWN: SELL_PERCENT_DOWN,
        LOG_LOGGLY_KEY: LOG_LOGGLY_KEY,
        LOG_LOGGLY_SUBDOMAIN: LOG_LOGGLY_SUBDOMAIN,
        LOG_MONGO_URI: LOG_MONGO_URI,
        LOG_MONGO_COLLECTION: LOG_MONGO_COLLECTION
    } });
var pubOpts = { uri: RABBIT_URI, socketType: rabbit.SocketType.PUB, queue: RABBIT_QUEUE_CMDS };
var pub = new rabbit.RabbitPub(pubOpts);
pub.connect();
pub.connectStream.subscribe(function () {
    return logger.write({ resource: "rabbit", oper: "connected", status: "success", opts: pubOpts });
}, function (err) {
    logger.write({ resource: "rabbit", status: "error", err: err, opts: pubOpts });
    process.exit(1);
});
var notifRequestOpts = { uri: RABBIT_URI, socketType: rabbit.SocketType.PUB, queue: RABBIT_QUEUE_REQUEST_NOTIF };
var pubNotifRequest = new rabbit.RabbitPub(notifRequestOpts);
pubNotifRequest.connect();
pubNotifRequest.connectStream.subscribe(function () {
    return logger.write({ resource: "rabbit", oper: "connected", status: "success", opts: notifRequestOpts });
}, function (err) {
    logger.write({ resource: "rabbit", status: "error", err: err, opts: notifRequestOpts });
    process.exit(1);
});
var quotesSubOpts = { uri: RABBIT_URI, socketType: rabbit.SocketType.SUB, queue: RABBIT_QUEUE_QUOTES };
var quotesSub = new rabbit.RabbitSub(quotesSubOpts);
quotesSub.connect();
quotesSub.stream.take(1).subscribe(function () {
    return logger.write({ resource: "rabbit", oper: "connected", status: "success", opts: quotesSubOpts });
}, function (err) {
    logger.write({ resource: "rabbit", oper: "connected", status: "error", err: err, opts: quotesSubOpts });
    process.exit(1);
});
var notifsSubOpts = { uri: RABBIT_URI, socketType: rabbit.SocketType.SUB, queue: RABBIT_QUEUE_NOTIFS };
var notifsSub = new rabbit.RabbitSub(notifsSubOpts);
notifsSub.connect();
notifsSub.stream.take(1).subscribe(function () {
    pubNotifRequest.write({ type: "INotifPortfolioChanged", query: { portfolio: PKG_NAME } });
    logger.write({ resource: "rabbit", oper: "connected", status: "success", opts: notifsSubOpts });
}, function (err) {
    logger.write({ resource: "rabbit", oper: "connected", status: "error", err: err, opts: notifsSubOpts });
    process.exit(1);
});
var quotesStream = quotesSub.stream.skip(1).share();
var buyOpts = {
    silverSurferStream: notifsSub.stream.skip(1)
        .filter(function (f) { return f.type == "INotifSilverSurferData"; }).share(),
    quotesStream: quotesStream,
    endOfDay: END_OF_DAY,
    buyPercent: BUY_PERCENT,
    cancelBuyPercent: CANCEL_BUY_PERCENT
};
var sellOpts = {
    portfolioStream: notifsSub.stream.skip(1)
        .filter(function (f) { return f.type == "INotifPortfolioChanged"; }).share(),
    quotesStream: quotesStream,
    sellPercentUp: SELL_PERCENT_UP,
    sellPercentDown: SELL_PERCENT_DOWN,
};
var buys = buyHandler.handleBuy(buyOpts);
var sells = sellHandler.handleSell(sellOpts);
Rx.Observable.merge(buyOpts.silverSurferStream, sellOpts.portfolioStream).subscribe(function (val) {
    return logger.write({ resource: "handle", status: "start", group: "notif", data: val });
}, function (err) {
    return logger.write({ resource: "handle", status: "error", group: "notif", err: err });
});
Rx.Observable.merge(buyHandler.handleBuy(buyOpts), sellHandler.handleSell(sellOpts)).subscribe(function (val) {
    return logger.write({ resource: "handle", status: "success", group: "notif", data: val });
}, function (err) {
    return logger.write({ resource: "handle", status: "error", group: "notif", err: err });
});
buys
    .filter(function (f) { return f.type == buyHandler.ResultType.buy; })
    .map(function (val) { return ({
    key: uuid.v4(),
    issuer: PKG_NAME,
    date: (new Date()).toISOString(),
    reason: val.reason,
    notif: val.notif,
    portfolio: PKG_NAME,
    trade: {
        ticket: val.notif.data.ticket,
        quantity: val.quantity,
        oper: "buy",
    }
}); })
    .subscribe(function (val) { return pub.write(val); });
sells.map(function (val) { return ({
    key: "[" + val.notif.key + "]_[" + val.position.ticket + "]_" + PKG_NAME,
    issuer: PKG_NAME,
    date: (new Date()).toISOString(),
    reason: val.reason,
    notif: val.notif,
    portfolio: PKG_NAME,
    trade: {
        ticket: val.position.ticket,
        quantity: val.position.quantity,
        oper: "sell",
    }
}); }).subscribe(function (val) { return pub.write(val); });
