function shouldSell(buyPrice, currentPrice, sellPercentUp, sellPercentDown) {
    if (currentPrice <= buyPrice * (1 - sellPercentDown / 100)) {
        return "Current price " + currentPrice + " less than buy price " + buyPrice + " by more than " + sellPercentDown + " percent (loss)";
    }
    else if (currentPrice >= buyPrice * (1 + sellPercentUp / 100)) {
        return "Current price " + currentPrice + " bigger than buy price " + buyPrice + " by more than " + sellPercentUp + " percent (gain)";
    }
    return null;
}
function handleSell(opts, scheduler) {
    var quotes = opts.quotesStream.selectMany(function (p) { return p; });
    var positionsStream = opts.portfolioStream.map(function (val) { return ({ notif: val, poss: val.data.positions.filter(function (f) { return f.quantity > 0; }) }); })
        .shareReplay(null, 1, scheduler);
    var qps = quotes.withLatestFrom(positionsStream, function (quote, tlp) {
        return ({ quote: quote, notif: tlp.notif, pos: tlp.poss.filter(function (f) { return f.ticket == quote.ticket; })[0] });
    })
        .filter(function (tlp) { return !!tlp.pos; });
    return qps.map(function (tlp) {
        return ({ data: tlp, reason: shouldSell(tlp.pos.value, tlp.quote.latestPrice, opts.sellPercentUp, opts.sellPercentDown) });
    })
        .filter(function (v) { return !!v.reason; })
        .map(function (tlp) { return ({
        quote: tlp.data.quote,
        notif: tlp.data.notif,
        position: tlp.data.pos,
        reason: tlp.reason
    }); }).share();
}
exports.handleSell = handleSell;
