FROM node:0.12

#RUN apt-get update
#RUN apt-get install libkrb5-dev
ADD package.json /app/
ADD dist /app/dist

WORKDIR /app
RUN npm install

CMD ["npm", "start"]
#ENTRYPOINT ["npm", "start"]
